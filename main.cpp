#include <iostream>
#include<time.h>
#include <cstdlib>
using namespace std;



template <typename type1,typename type2>

class Knot{
public:
Knot* parent;
Knot* left;
Knot* right;
int key;
string value;
Knot(int a,string b){
    key=a; value=b;
    left=right=parent=NULL;
}
};

template <typename type1,typename type2>
class BinaryTree{
public:
    Knot *kn;


    BinaryTree();
    BinaryTree(int k,string v);
    void Add(int key, string value);
    void InOrder(Knot*);
    void remove(int k);
    Knot* search(int k);
    Knot* min(Knot*);
    Knot* max(Knot*);
};

BinaryTree::BinaryTree(){
kn=NULL;
};

BinaryTree::BinaryTree(int k,string v){
 kn=new Knot(k,v);
 cout<<kn->value<<endl;
};

void BinaryTree::Add(int k,string v){
    if(kn==NULL){
    kn=new Knot(k,v);
    }
    else{

    Knot *position=kn;
    Knot *previous=NULL;
    while(position!=NULL){

        previous=position;
        position = (k < position->key) ? position->left : position->right;
    }
//Tworzenie nowego obiektu ze wskaznikiem na rodzica
    Knot *newby;
    newby = new Knot(k,v);
    newby->parent = previous;

    newby->left = newby->right = NULL;

    if(!previous) kn = newby;

    else if
        (k < previous->key) previous->left  = newby;

    else
        previous->right = newby;


    cout<<newby->key<<"  ";
    cout<<newby->parent->key<<endl;
    }
};

void BinaryTree::InOrder(Knot *v){
       if(v){
        InOrder(v->left);
        cout << v->value << endl;
        InOrder(v->right);
       }
//cout<< "Done!"<<endl;
};

Knot* BinaryTree::search(int k){

  Knot *x = kn;

  while((x) && (x->key != k))
    x = (k < x->key) ? x->left : x->right;

  return x;
}

Knot* BinaryTree::max(Knot *K){
  Knot* x = K;

  while(x->right) x = x->right;

  return x;
};

Knot* BinaryTree::min(Knot* K){
  Knot* x = K;

  while(x->left) x = x->left;

  return x;
};


void BinaryTree::remove(int k){
        if(k==kn->key){

        kn->key = min(kn->right)->key;
        kn->value = min(kn->right)->value;
        kn->parent=NULL;
        Knot* temp =min(kn->right)->parent;
        delete min(kn->right);
        temp->left=NULL;
        cout<< "USUNIETE";
        return ;
        }

    Knot *x = search(k);
    Knot *y = x->parent;

    if(x->left==NULL && x->right==NULL)
    {

        if(x->key < y->key) {
            delete y->left;
            y->left=NULL;}
        else {
            delete y->right;
            y->right=NULL;}
    }
    else if(x->left==NULL){
        if(y->right == x){
                y->right = x->right;
                y->right->parent =y;
                delete x;

                }
        else {y->left = x->right; delete x; y->left ->parent=y;}
        }
    else if(x->right==NULL){
        if(y->right == x){y->right = x->left; delete x; y->right->parent =y;}
        else {y->left = x->left; delete x; y->left->parent =y;}
    }
    else if(x->left!=NULL && x->right!=NULL){
        Knot *temporary = min(x->right);
        temporary->right = x->right;
        temporary->left = x->left;
        if(x->key < y->key){y->left = temporary;}
        else {y->right = temporary;}
        delete x;
       // temporary->parent->left=NULL;
        temporary->parent=y;
        if(temporary->right == temporary){temporary->right=NULL;}
        if(temporary->left == temporary){temporary->left=NULL;}
    }
};






int main()
{
    cout << "Hello world!" << endl;

    BinaryTree T(5,"korzen");
    T.Add(2,"2");
    T.Add(3,"3");
    T.Add(10,"10");
    T.Add(7,"7");
    T.Add(12,"12");
   // T.InOrder(T.kn);
    T.remove(5);

    T.InOrder(T.kn);



    return 0;
}
